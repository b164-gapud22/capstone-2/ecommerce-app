const mongoose = require("mongoose");
const productSchema = new mongoose.Schema({
	productName: {
		type: String,
		required: [true, "Product name is required"]
	},
	description: {
		type: String,
		required: [true, "Description is required"]
	},
	price: {
		type: Number,
		required: [true, "Price is required"]
	},
	stock: {
		type: Number,
		required: [true, "stock is required"]
	},
	images: {
		type: Object,
	},

	category: {
		type: String,
		required: [true, "Category is required"]
	},

	isActive: {
		type: Boolean,
		default: true
	},

	isInCart: {
		type: Boolean,
		default: false
	},

	sold: {
		type: Number,
		default: 0
	},

	order: [
		{
			userId: {
				type: String,
				required: [true, "UserId is required"]
			},
			quantity: {
				type: Number,
				required: [true, "Quantity is required"]
			},
			subTotal: {
				type: Number,
				required: [true, "Subtotal is required"]
			}
		}
	] 
	
})



module.exports = mongoose.model("Product", productSchema);