// to access the model
const User = require("../model/user");
const Product = require("../model/product");


// encrpted password and authorization
const bcrypt = require('bcrypt');
const auth = require('../auth');



// User Registration
module.exports.userRegistration = (reqBody) => {
	return User.find({ email: reqBody.email }).then(result => {
		if(result.length > 0){
			return ({message: "User already exists."});
		}else{
			// if no duplicate found
			// Create new user
			let newUser = new User ({
				firstName: reqBody.firstName,
				lastName: reqBody.lastName,
				email: reqBody.email,
				password: bcrypt.hashSync(reqBody.password, 10),
				mobileNumber: reqBody.mobileNumber
			})

			// Save new user
			return newUser.save().then((user, error) => {
				// if user registration failed
				if(error){
					return false;
				}else{
					// User registration success
					return true;
				}
			})
		}
	})
}



// User authentication
module.exports.userLogin = (reqBody) => {
	// Find the email of the user
	return User.findOne({ email: reqBody.email }).then( result => {
		// if not found
		if(result == null){
			return false;
		}else{
			// Compare the provided password with the encrypted password.
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			// if the password match
			if(isPasswordCorrect){
				return { accessToken : auth.createAccessToken(result.toObject()) }
			}else{
				// password does not match
				return false;
			}
		}
	})
}


// get details of user
module.exports.getProfile = (data) => {
	return User.findById(data).then(result => {

		result.password = "";

		return result;
	})
}







// Set user as admin
module.exports.setAdmin = (reqBody) => {
	// Find the email of the user
	return User.findOne({ email: reqBody.email }).then(result => {
		// if not found
		if(result == null){
			return false;
		}else{
			// if found.
			result.isAdmin = true;

			// save the changes
			return result.save().then((result, error) => {
				if(error){
					return false;
				}else{
					return true;
				}
			})

		}
	})
}





// Make an order
module.exports.makeOrder = (data) => {
	// Check if the stock is available
	return Product.findById(data.productId).then(result => {
		// if the stock is available
		if(result.stock >= data.quantity){

			// Update the user
			let isUserUpdated = User.findById(data.userId).then(user => {

				user.forCheckout.push({
					productId: data.productId,
					productName: result.productName
				});

				// save
				return user.save().then((user, err) => {
					if(err){
						return false;
					}else{
						return true;
					}
				})
			});


			// Update the product
			let isProductUpdated = Product.findById(data.productId).then(product => {

					product.order.push({
						userId: data.userId,
						quantity: data.quantity,
						subTotal: data.quantity * product.price
					})
					// save
					return product.save().then((product, err) => {
						if(err){
							return false;
						}else{
							return true;
						}
					})
			});


			// validate
			if(isProductUpdated && isUserUpdated){
				return true;
			}else{
				return false;
			};


		}else{
			return 'Not enough stock.'
		}
	})


};





// Retrieve aunthenticated user's order
module.exports.userOrder = (data) => {

	return User.findById(data.userId, {email: 1, forCheckout: 1, _id: 0}).then((result, err) => {
		if(err){
			return false;
		}else{
			return result
		}
	})
}





// Retrieve all orders(admin only)
module.exports.allOrders = () => {
	return User.find({"forCheckout.status": "pending"}).then(result => {
		
		return result;

	})

}



// Get all users
module.exports.allUsers = () => {
	return User.find({}).then(result => {
		return result;
	})
}